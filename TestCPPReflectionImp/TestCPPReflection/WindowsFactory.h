#pragma once
#include "common.h"
#include "IFactory.h"

namespace DependencyLocate
{
	class WindowsFactory : public IFactory
	{
	public:
		IWindow* MakeWindow();
		IButton* MakeButton();
	};
}
