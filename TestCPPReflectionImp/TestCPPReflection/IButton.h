#pragma once
#include "common.h"

namespace DependencyLocate
{
	interface IButton
	{
	public:
		virtual string ShowInfo() = 0;
		virtual ~IButton(){}
	};
}