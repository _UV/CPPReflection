#include "WindowsFactory.h"
#include "WindowsButton.h"
#include "WindowsWindow.h"

using namespace DependencyLocate;


IWindow* WindowsFactory::MakeWindow()
{
	return new WindowsWindow();
}

IButton* WindowsFactory::MakeButton()
{
	return new WindowsButton();
}
