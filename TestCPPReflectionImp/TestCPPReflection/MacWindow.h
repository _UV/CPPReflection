#pragma once
#include "common.h"
#include "IWindow.h"

namespace DependencyLocate
{
	class MacWindow : public IWindow
	{
	public:
		~MacWindow();
		string ShowInfo();
	};
}