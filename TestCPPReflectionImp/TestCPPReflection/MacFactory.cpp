#include "MacFactory.h"
#include "MacWindow.h"
#include "MacButton.h"

using namespace DependencyLocate;

IWindow* MacFactory::MakeWindow()
{
	return new MacWindow();
}

IButton* MacFactory::MakeButton()
{
	return new MacButton();
}
